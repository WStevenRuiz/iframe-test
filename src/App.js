import React from 'react';
import Grid from '@material-ui/core/Grid';
import './App.css';

const App = () => {
  return (
    <Grid container>
      <Grid md={12} xs={12} style={{ height: '100vh' }}>
        <iframe
          title='iframe-test'
          src='https://player.vimeo.com/video/408868777'
          width='100%'
          height='100%'
          frameborder='0'
          allow='autoplay; fullscreen'
          allowfullscreen
        ></iframe>
      </Grid>
    </Grid>
  );
};

export default App;
